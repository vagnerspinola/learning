from tokenize import String

from class_utils.decorator import is_in_group
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from core.models import Utente
from corso.models import Anello1, Anello2
from valutazione.models import Skill, User_Skill


def index(request):
    data_to_template = {
        'nav' 		: 'home',
        'subnav' 		: '',
        'title_big' 		: 'Dashboard',
        'title_small' 	: "home page",
        'html_hear'	: '<li><a href="/">Dashboard</a></li>',
        'users': User.objects.get(username=request.user)
    }
    return render(request, 'core/site_index.html', data_to_template)


def get_level(livelo):
    if livelo == 'I':
        level = 'Intermediate'
    elif livelo == 'M':
        level = 'Master'
    elif livelo == 'S':
        level = 'Senior'
    elif livelo == 'J':
        level = 'Junior'
    else:
        level = 'Not Applicable'

    return level


def get_line_table(username, level, comp, vendor, tech):
    lista_worker = '''
                            <tr>
                                <td>{}</td>
                                <td>{}</td>
                                <td>{}</td>
                                <td>{}</td>
                                <td>{}</td>
                            </tr>
                            '''.format(comp, vendor, tech, username, level)
    return lista_worker


def get_head_table(lista_worker):
    lista_worker_aux = '''
                            <table>
                                <thead>
                                    <tr>
                                        <th width=200>Operation Area</th>
                                        <th width=200>Vendor</th>
                                        <th width=300>Technologia</th>
                                        <th width=200>Professionista</th>
                                        <th width=100>Livelo</th>
                                        
                                    </tr>
                                </thead>
                                <tbody style="overflow-y: auto;">
                                    {}
                                </tbody>
                            </table>
                        '''.format(lista_worker)
    return lista_worker_aux


@is_in_group( group='diretore' )
def admin_index(request):
    dict_amb = {}
    dict_skills = {"name": "Dgs S.p.a",
                   "children": []}

    lista_ambito = Anello1.objects.all()
    for single_ambito in lista_ambito:
        ven = []
        lista_worker_amb = ''
        lista_comp_amb = ''
        lista_skills = Skill.objects.filter(ambito=single_ambito.id)

        if len(lista_skills) > 0:
            for single_vendor in lista_skills:
                if single_vendor.vendor.desc not in ven:
                    ven.append(single_vendor.vendor.desc)

            l = []
            lista_worker_ven = ''
            for single_vendor in ven:
                lista_tech = []
                tec = []
                lista_worker_ven = ''
                lista_comp_ven = ''
                lista_skills_technologia = Skill.objects.filter(vendor__desc=single_vendor,
                                                                ambito=single_ambito)

                if len(lista_skills_technologia) > 0:
                    for single_tech in lista_skills_technologia:
                        if single_tech.technology.desc not in tec:
                            tec.append(single_tech.technology.desc)

                for single_tech in tec:
                    lista_capacity = []
                    capacity = []

                    lista_skills_cap = Skill.objects.filter(technology__desc=single_tech,
                                                            vendor__desc=single_vendor,
                                                            ambito=single_ambito)
                    if len(lista_skills_cap) > 0:
                        for single_cap in lista_skills_cap:
                            if single_cap.capacity.desc not in capacity:
                                capacity.append(single_cap.capacity.desc)

                    lista_cap = []
                    lista_worker_tech = ''
                    lista_comp_tech = ''
                    for cap in capacity:
                        legitimita = []
                        lista_leg = []
                        lista_worker_cap = ''
                        lista_comp_cap = ''
                        lista_skills_leg = Skill.objects.filter(capacity__desc=cap,
                                                                technology__desc=single_tech,
                                                                vendor__desc=single_vendor,
                                                                ambito=single_ambito)

                        if len(lista_skills_leg) > 0:
                            for single_leg in lista_skills_leg:
                                if single_leg.legitimita.desc not in legitimita:
                                    legitimita.append(single_leg.legitimita.desc)

                        for leg in legitimita:
                            module = []
                            lista_mod = []
                            lista_worker_mod = ''
                            lista_comp_mod = ''
                            lista_skills_mod = Skill.objects.filter(legitimita__desc=leg,
                                                                    capacity__desc=cap,
                                                                    technology__desc=single_tech,
                                                                    vendor__desc=single_vendor,
                                                                    ambito=single_ambito)

                            if len(lista_skills_mod) > 0:
                                for single_mod in lista_skills_mod:
                                    if single_mod.legitimita.desc not in module:
                                        module.append(single_mod.module.desc)

                            for mod in module:
                                competence = []
                                lista_comp = []
                                lista_worker_comp = ''
                                lista_comp_comp = ''
                                lista_skills_comp = Skill.objects.filter(module__desc=mod,
                                                                         legitimita__desc=leg,
                                                                         capacity__desc=cap,
                                                                         technology__desc=single_tech,
                                                                         vendor__desc=single_vendor,
                                                                         ambito=single_ambito)

                                if len(lista_skills_comp) > 0:
                                    for single_comp in lista_skills_comp:
                                        if single_comp.legitimita.desc not in competence:
                                            competence.append(single_comp.competence.desc)

                                for comp in competence:
                                    skill_pk = Skill.objects.get(competence__desc=comp,
                                                                   module__desc=mod,
                                                                   legitimita__desc=leg,
                                                                   capacity__desc=cap,
                                                                   technology__desc=single_tech,
                                                                   vendor__desc=single_vendor,
                                                                   ambito=single_ambito).pk

                                    worker = User_Skill.objects.filter(skill__pk=skill_pk).order_by('user__user__username', 'livelo')

                                    for w in worker:
                                        if w.user.user.username != "None":
                                            level = get_level(w.livelo)
                                            # lista_worker_comp += get_line_table(w.user.user.username, level, w.skill.competence.desc)
                                            # lista_worker_mod += get_line_table(w.user.user.username, level, w.skill.competence.desc)
                                            # lista_worker_leg += get_line_table(w.user.user.username, level, w.skill.competence.desc)

                                            lista_worker_cap += get_line_table(w.user.user.username, level, w.skill.competence.desc, w.skill.legitimita.desc, w.skill.module.desc)
                                            lista_worker_tech += get_line_table(w.user.user.username, level, w.skill.competence.desc, w.skill.legitimita.desc, w.skill.module.desc)
                                            lista_worker_ven += get_line_table(w.user.user.username, level, w.skill.competence.desc, w.skill.legitimita.desc, w.skill.module.desc)
                                            lista_worker_amb += get_line_table(w.user.user.username, level, w.skill.competence.desc, w.skill.legitimita.desc, w.skill.module.desc)

                                    # Anello 7
                                    '''lista_worker_comp = get_head_table(lista_worker_comp)
        
                                    if leg != '':
                                        lista_comp.append({"name": comp,
                                                          "lista_worker_comp": lista_worker_comp,
                                                          "value": 1})'''

                                # Anello 6
                                '''lista_worker_mod = get_head_table(lista_worker_mod)

                                if leg != '':
                                    lista_mod.append({"name": mod,
                                                      "lista_worker_mod": lista_worker_mod,
                                                      "value": 1})'''
                            # Anello 5
                            '''lista_worker_leg = get_head_table(lista_worker_leg)

                            if leg != '':
                                lista_leg.append({"name": leg,
                                                  "lista_worker_leg": lista_worker_leg,
                                                  "value": 1})'''
                        # Anello 4
                        lista_worker_cap = get_head_table(lista_worker_cap)


                        if cap != '':
                                lista_cap.append({"name": cap,
                                                  "lista_worker_cap": lista_worker_cap,
                                                  "value": 1})
                    # Anello 3
                    lista_worker_tech = get_head_table(lista_worker_tech)

                    if single_tech != '':

                        if len(lista_cap) > 0:
                            lista_tech.append({"name": single_tech,
                                               "list_worker_tech": lista_worker_tech,
                                               "children": lista_cap})
                        else:
                            lista_tech.append({"name": single_tech,
                                               "list_worker_tech": lista_worker_tech,
                                               "value": 1})
                # Anello 2
                lista_worker_ven = get_head_table(lista_worker_ven)

                if len(lista_tech) > 0:
                    l.append({"name": single_vendor,
                              'lista_work_ven': lista_worker_ven,
                              "logo":  Anello2.objects.get(desc=single_vendor).logo.url,
                              "children": lista_tech})
                else:
                    if single_vendor != '':
                        l.append({"name": single_vendor,
                                  'lista_work_ven': lista_worker_ven,
                                  "logo": Anello2.objects.get(desc=single_vendor).logo.url,
                                  "value": 1})
            # Anello 1
            lista_worker_amb = get_head_table(lista_worker_amb)

            if len(l) > 0:
                dict_amb = {"name": single_ambito.desc,
                            'lista_work_amb': lista_worker_amb,
                            "children": l}
            else:
                dict_amb = {"name": single_ambito.desc,
                            'lista_work_amb': lista_worker_amb,
                            "value": 1}

        dict_skills['children'].append(dict_amb)

    dicionario = []
    dicionario.append(dict_skills)

    risp = []
    risp2 = []

    # get quantiti of responsabile nella azienda
    r = Utente.objects.all().values_list('resposabile', flat=True).distinct()
    for lista_ambito in r:
        risp.append(User.objects.get(pk=lista_ambito).username)
        risp2.append(User.objects.get(pk=lista_ambito))

    # get quantiti of user per risponsabile
    graf_risponsabile = []
    for r in risp2:
        lista_ambito = Utente.objects.filter(resposabile=r)
        graf_risponsabile.append(len(lista_ambito))

    # get quantity of ambito in DB
    skills = Anello1.objects.all()
    skill = []
    for lista_ambito in skills:
        skill.append(lista_ambito.desc)

    # get quantity of user per ambito
    graf_skill = []
    for r in skills:
        lista_ambito = Skill.objects.filter(ambito=r.id)
        graf_skill.append(len(lista_ambito))

    # get livelo
    level = []
    le = User_Skill.objects.all().values_list('livelo', flat=True).distinct()
    for lista_ambito in le:
        level.append(lista_ambito)

    graf_level = []
    for r in le:
        lista_ambito = User_Skill.objects.filter(livelo=r)
        graf_level.append(len(lista_ambito))

    data_to_template = {
        'nav' 		: 'home',
        'subnav' 		: '',
        'title_big' 		: 'Dashboard',
        'title_small' 	: "home page",
        'html_hear'	: '<li><a href="/">Dashboard</a></li>',
        'graf_data': graf_risponsabile,
        'graf_name': risp,
        'skill': skill,
        'graf_skill': graf_skill,
        'level': level,
        'graf_level': graf_level,
        'dict': dicionario,
    }

    return render(request, 'core/body-index.html', data_to_template)


def reset(request):
    return redirect('admin/password_reset_form.html')


@login_required()
def myprofile(request):

    data_to_template = {
			'nav' 			: '',
			'subnav' 		: '',
			'title_big' 	: 'Il mio profilo',
			'title_small' 	: "",
			'html_header'	: '<li><a href="/myprofile/">Il mio profilo</a></li>',
		}

    return render(request, 'core/myprofile.html',  data_to_template)