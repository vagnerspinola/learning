# corso admin

from django.contrib import admin
from corso.models import *


class AmbitoOption(admin.ModelAdmin):
    list_display   = ('id', 'desc')
    ordering       = ('id', )
    search_fields = ('id', 'desc')
    save_on_top    = True


class VendorOption(admin.ModelAdmin):
    list_display = ('id', 'desc', 'logo')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class TechnologyOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class CapacityOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class LegitimitaOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class ModuleOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class CompetenceOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class CertificazioneOption(admin.ModelAdmin):
    list_display = ('id', 'name', 'desc', 'start', 'end', 'technology', 'vendor', 'image')
    ordering = ('id',)
    search_fields = ('id', 'name')
    save_on_top = True


class CorsoOption(admin.ModelAdmin):
    list_display = ('id', 'name', 'desc', 'start', 'end', 'technology', 'vendor', 'image')
    ordering = ('id',)
    search_fields = ('id', 'name')
    save_on_top = True


admin.site.register(Technology, ModuleOption)
admin.site.register(Competence, CompetenceOption)
admin.site.register(Anello4, CapacityOption)
admin.site.register(Anello3, TechnologyOption)
admin.site.register(Anello2, VendorOption)
admin.site.register(Anello1, AmbitoOption)
admin.site.register(Certificazione, CertificazioneOption)
admin.site.register(Corso, CorsoOption)
admin.site.register(Vendor, LegitimitaOption)