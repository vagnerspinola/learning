# corso models

from django.db import models


class Anello1(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Anello1"
        verbose_name_plural = "Anelli1"


class Anello2(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')
    logo = models.ImageField(upload_to='logo_vendor', default='logo_vendor/logo.jpg')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Anello2"
        verbose_name_plural = "Anelli2"


class Anello3(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Anello3"
        verbose_name_plural = "Anelli3"


class Anello4(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Anello4"
        verbose_name_plural = "Anelli4"


class Vendor(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Vendor"
        verbose_name_plural = "Vendors"


class Technology(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Technology"
        verbose_name_plural = "Technologys"


class Competence(models.Model):
    desc = models.CharField(max_length=100, unique=True, blank=True, default='')

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Competence"
        verbose_name_plural = "Competences"


class Certificazione(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField()
    image = models.ImageField(upload_to='certificazioni')
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    technology = models.ForeignKey(Anello3, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Anello2, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Certificazione"
        verbose_name_plural = "Certificazioni"


class Corso(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField()
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    image = models.ImageField(upload_to='corsi')
    technology = models.ForeignKey(Anello3, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Anello2, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Corso"
        verbose_name_plural = "Corsi"



