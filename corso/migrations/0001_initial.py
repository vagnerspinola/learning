# Generated by Django 2.1.5 on 2019-05-06 15:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Anello1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Anello1',
                'verbose_name_plural': 'Ambitos',
            },
        ),
        migrations.CreateModel(
            name='Anello4',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Anello4',
                'verbose_name_plural': 'Capacitys',
            },
        ),
        migrations.CreateModel(
            name='Certificazione',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('desc', models.TextField()),
                ('image', models.ImageField(upload_to='certificazioni')),
                ('start', models.DateTimeField(blank=True, null=True)),
                ('end', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Certificazione',
                'verbose_name_plural': 'Certificazioni',
            },
        ),
        migrations.CreateModel(
            name='Competence',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Competence',
                'verbose_name_plural': 'Competences',
            },
        ),
        migrations.CreateModel(
            name='Corso',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('desc', models.TextField()),
                ('start', models.DateTimeField(blank=True, null=True)),
                ('end', models.DateTimeField(blank=True, null=True)),
                ('image', models.ImageField(upload_to='corsi')),
            ],
            options={
                'verbose_name': 'Corso',
                'verbose_name_plural': 'Corsi',
            },
        ),
        migrations.CreateModel(
            name='Vendor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Vendor',
                'verbose_name_plural': 'Legitimitas',
            },
        ),
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Technology',
                'verbose_name_plural': 'Modules',
            },
        ),
        migrations.CreateModel(
            name='Anello3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
            ],
            options={
                'verbose_name': 'Anello3',
                'verbose_name_plural': 'Technologys',
            },
        ),
        migrations.CreateModel(
            name='Anello2',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('desc', models.CharField(blank=True, default='', max_length=100, unique=True)),
                ('logo', models.ImageField(default='logo_vendor/logo.jpg', upload_to='logo_vendor')),
            ],
            options={
                'verbose_name': 'Anello2',
                'verbose_name_plural': 'Vendors',
            },
        ),
        migrations.AddField(
            model_name='corso',
            name='technology',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corso.Anello3'),
        ),
        migrations.AddField(
            model_name='corso',
            name='vendor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corso.Anello2'),
        ),
        migrations.AddField(
            model_name='certificazione',
            name='technology',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corso.Anello3'),
        ),
        migrations.AddField(
            model_name='certificazione',
            name='vendor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='corso.Anello2'),
        ),
    ]
