from django.conf.urls import url, include
from . import views


app_name = 'valutazione'

urlpatterns = [
    url(r'^valutazione/$', views.cert, name='cert'),
    url(r'^level/$', views.level, name='level'),
    url(r'^upload/$', views.upload, name='upload'),

]
