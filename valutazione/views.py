# valutazione views

from django.shortcuts import render
from django.contrib.auth.models import User
from core.models import Utente
from corso.models import *
from valutazione.models import Skill, User_Skill, Evaluation
from django.db import transaction


def cert(request):
    data_to_template = {
        'nav' 		: 'val',
        'subnav' 		: 'val-cert',
        'title_big' 		: 'Dashboard',
        'title_small' 	: "",
        'html_hear'	: '<li><a href="/">Dashboard</a></li>',
        'certs': Evaluation.objects.all().order_by('utente', 'corso', 'certification'),
    }
    return render(request, 'valutazione/cert.html', data_to_template)


def level(request):

    # get quantiti of responsabile nella azienda
    risp = []
    r = Utente.objects.all().values_list('resposabile', flat=True).distinct()
    for x in r:
        risp.append(User.objects.get(pk=x))

    if request.method == 'POST':
        post = request.POST

        if 'user' in request.POST and post['user'] != '':
            user = User_Skill.objects.filter(user=post['user']).order_by('user', 'skill')
        else:
            user = User_Skill.objects.all().order_by('user', 'skill')
    else:
        user = User_Skill.objects.all().order_by('user', 'skill')

    data_to_template = {
        'nav' 	: 'val',
        'subnav' 		: 'val-level',
        'title_big' 		: 'Dashboard',
        'title_small' 	: "",
        'html_hea'	: '<li><a href="/">Dashboard</a></li>',
        'skills': user,
        'lista_user': Utente.objects.all().order_by('user__username'),
        'lista_risp': risp,
    }

    return render(request, 'valutazione/level.html', data_to_template)



def upload(request):
    data_to_template = {
        'nav' 		: 'val',
        'subnav' 		: 'val-cert',
        'title_big' 		: 'Dashboard',
        'title_small' 	: "",
        'html_hear'	: '<li><a href="/">Dashboard</a></li>',
    }

    file = 'skillcompleto.csv'
    with open(file) as f:
        lines = f.readlines()

    lines = [line.rstrip('\n') for line in open(file)]
    i = 0
    for line in lines:
        line = line.split(',')

        check_username = line[0].replace(' ', '')
        try:
            user_id = User.objects.get(username=check_username)
        except:
            user_id = None

        if user_id is None:
            user = User()
            user.username = check_username
            user.password = 'Dgsspa1234567890'
            user.save()
            transaction.commit()
            print('saved dipendente....', check_username)

        check_risponsabile = line[9].replace(' ', '')
        try:
            user_id = User.objects.get(username=check_risponsabile)
        except:
            user_id = None

        if user_id is None:
            user = User()
            user.username = check_risponsabile
            user.password = 'Dgsspa1234567890'
            user.save()
            transaction.commit()
            print('saved responsabile....', check_risponsabile)

        username = User.objects.get(username=line[0].replace(' ', ''))
        resp = User.objects.get(username=line[9].replace(' ', ''))
        if resp:
            if username:
                if not Utente.objects.filter(user=username):
                    ut = Utente()
                    ut.user = username
                    ut.resposabile = resp
                    ut.save()
                    transaction.commit()
                    print('saved utente....', username)

        try:
            check_ambito = Anello1.objects.get(desc=line[1])
        except:
            check_ambito = None

        if check_ambito is None:
            a = Anello1()
            a.desc = line[1]
            a.save()
            transaction.commit()
            print('saved ambito....', line[1])
            check_ambito = Anello1.objects.get(desc=line[1])

        try:
            check_vendor = Anello2.objects.get(desc=line[2])
        except:
            check_vendor = None

        if check_vendor is None:
            v = Anello2()
            v.desc = line[2]
            v.save()
            transaction.commit()
            print('saved vendor....', line[2])
            check_vendor = Anello2.objects.get(desc=line[2])

        try:
            check_technology = Anello3.objects.get(desc=line[3])
        except:
            check_technology = None

        if check_technology is None:
            t = Anello3()
            t.desc = line[3]
            t.save()
            transaction.commit()
            print('saved technology....', line[3])
            check_technology = Anello3.objects.get(desc=line[3])

        try:
            check_capacity = Anello4.objects.get(desc=line[4])
        except:
            check_capacity = None

        if check_capacity is None:
            c = Anello4()
            c.desc = line[4]
            c.save()
            transaction.commit()
            print('saved capacity....', line[4])
            check_capacity = Anello4.objects.get(desc=line[4])


        try:
            check_legitimita = Vendor.objects.get(desc=line[5])
        except:
            check_legitimita = None

        if check_legitimita is None:
            c = Vendor()
            c.desc = line[5]
            c.save()
            transaction.commit()
            print('saved capacity....', line[5])
            check_legitimita = Vendor.objects.get(desc=line[5])

        try:
            check_module = Technology.objects.get(desc=line[6])
        except:
            check_module = None

        if check_module is None:
            c = Technology()
            c.desc = line[6]
            c.save()
            transaction.commit()
            print('saved Technology....', line[6])
            check_module = Technology.objects.get(desc=line[6])

        try:
            check_competence = Competence.objects.get(desc=line[7])
        except:
            check_competence = None

        if check_competence is None:
            c = Competence()
            c.desc = line[7]
            c.save()
            transaction.commit()
            print('saved Competence....', line[7])
            check_competence = Competence.objects.get(desc=line[7])

        try:

            skill_id = Skill.objects.get(   ambito=check_ambito,
                                            vendor=check_vendor,
                                            technology=check_technology,
                                            capacity=check_capacity,
                                            legitimita=check_legitimita,
                                            module=check_module,
                                            competence=check_competence)
        except:
            skill_id = None

        if skill_id is None:
            sk = Skill()
            sk.ambito = check_ambito
            sk.vendor = check_vendor
            sk.technology = check_technology
            sk.capacity = check_capacity
            sk.legitimita = check_legitimita
            sk.module = check_module
            sk.competence = check_competence
            sk.save()
            transaction.commit()
            print('saved Skill....', line[1], line[2], line[3], line[4], line[5], line[6], line[7])
            skill_id = Skill.objects.get(ambito=check_ambito,
                                         vendor=check_vendor,
                                         technology=check_technology,
                                         capacity=check_capacity,
                                         legitimita=check_legitimita,
                                         module=check_module,
                                         competence=check_competence)

        try:
            us = User_Skill.objects.get(user_id=username.id, skill=skill_id)
        except:
            us = None

        if skill_id is not None:
            if us is None:
                try:
                    i += 1
                    us = User_Skill()
                    us.user = Utente.objects.get(user_id=username.id)
                    us.skill = skill_id
                    us.livelo = line[8]
                    us.save()
                    print('{} saving user skill - {}'.format(i, line))
                except:
                    i += 1
                    print('{} ERROR saving user skill - {}'.format(i, line))

        else:
            print('cheking skill id - fail')
            if check_ambito and check_vendor and check_technology and check_capacity and check_legitimita and check_module and check_competence:
                skill = Skill()
                skill.ambito = check_ambito
                skill.vendor = check_vendor
                skill.technology = check_technology
                skill.capacity = check_capacity
                skill.legitimita = check_legitimita
                skill.module = check_module
                skill.competence = check_competence
                skill.save()
                print('Saved skill ')
                skill_id = Skill.objects.get(ambito=check_ambito,
                                             vendor=check_vendor,
                                             technology=check_technology,
                                             capacity=check_capacity,
                                             legitimita=check_legitimita,
                                             module=check_module,
                                             competence=check_competence)

                try:
                    i += 1
                    us = User_Skill()
                    us.user = Utente.objects.get(user_id=username.id)
                    us.skill = skill_id
                    us.livelo = line[8]
                    us.save()
                    print('{} saving user skill - {}'.format(i, line))
                except:
                    i += 1
                    print('{} ERROR saving user skill - {}'.format(i, line))

    return render(request, 'valutazione/cert.html', data_to_template)
