# valutazione admin

from django.contrib import admin
from .models import *


class SkillOption(admin.ModelAdmin):
    list_display   = ('id', 'ambito', 'vendor', 'technology', 'capacity', 'legitimita', 'module', 'competence')
    ordering       = ('id', )
    search_fields = ('ambito', 'vendor', 'technology', 'user')
    save_on_top    = True


class User_SkillOption(admin.ModelAdmin):
    list_display = ('id', 'user', 'skill', 'livelo')
    ordering = ('id',)
    search_fields = ('id', 'user', 'skill', 'livelo')
    save_on_top = True


class EvaluationOption(admin.ModelAdmin):
    list_display   = ('id', 'utente', 'certification', 'corso', 'previsione', 'conseguimento', 'scadenza', 'status')
    ordering       = ('id', )
    search_fields = ('ambito', 'utente', 'certification', 'corso')
    save_on_top    = True


admin.site.register(User_Skill, User_SkillOption)
admin.site.register(Evaluation, EvaluationOption)
admin.site.register(Skill, SkillOption)
