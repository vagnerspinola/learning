from django.apps import AppConfig


class ValutazioneConfig(AppConfig):
    name = 'valutazione'
