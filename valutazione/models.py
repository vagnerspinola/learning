# valutazione models

from django.db import models
from core.models import Utente
from corso.models import *

LIVELO = (('M', 'MASTER'), ('S', 'SENIOR'), ('L', 'INTERMEDIATE'), ('J', 'JUNIOR'), ('N/A', 'NOT APPLICABLE'),)
STATUS = (('APROVATO', 'APROVATO'), ('NEGGATO', 'NEGGATO'), ('ANALISE', 'ANALISE'),)


class Skill(models.Model):
    ambito = models.ForeignKey(Anello1, on_delete=models.CASCADE)
    vendor = models.ForeignKey(Anello2, on_delete=models.CASCADE)
    technology = models.ForeignKey(Anello3, on_delete=models.CASCADE)
    capacity = models.ForeignKey(Anello4, on_delete=models.CASCADE)
    legitimita = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    module = models.ForeignKey(Technology, on_delete=models.CASCADE)
    competence = models.ForeignKey(Competence, on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {} - {} - {} - {} - {} - {}'.format(self.ambito.desc,
                                                         self.vendor.desc,
                                                         self.technology.desc,
                                                         self.capacity.desc,
                                                         self.legitimita.desc,
                                                         self.module.desc,
                                                         self.competence.desc)

    class Meta:
        unique_together = ('ambito', 'vendor', 'technology', 'capacity', 'legitimita', 'module', 'competence')
        verbose_name = "Skill"
        verbose_name_plural = "Skills"


class User_Skill(models.Model):
    user = models.ForeignKey(Utente, on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    livelo               = models.CharField(max_length=3,
                                            choices=LIVELO,
                                            verbose_name='Livelo',
                                            default='N/A')

    def __str__(self):
        return '{} - {} - {}'.format(self.skill.ambito.desc, self.skill.vendor.desc, self.skill.technology.desc, self.skill.capacity.desc)

    class Meta:

        unique_together = ('user', 'skill',)
        verbose_name = "User Skill"
        verbose_name_plural = "User Skills"


class Evaluation(models.Model):
    certification = models.ForeignKey(Certificazione, on_delete=models.CASCADE, null=True, blank=True)
    corso = models.ForeignKey(Corso, on_delete=models.CASCADE, null=True, blank=True)
    utente = models.ForeignKey(Utente, on_delete=models.CASCADE)
    previsione = models.DateTimeField(null=True, blank=True)
    conseguimento = models.DateTimeField(null=True, blank=True)
    scadenza = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=10,
                              choices=STATUS,
                              verbose_name='Status',
                              default='ANALISE')

    def __str__(self):
        return self.status

    class Meta:
        verbose_name = "Evaluation"
        verbose_name_plural = "Evaluations"