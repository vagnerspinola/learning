from django.db import models
from django.contrib.auth.models import User


class Rank(models.Model):
    desc = models.CharField(max_length=500)

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Rank"
        verbose_name_plural = "Ranks"


class Position(models.Model):
    desc = models.CharField(max_length=500)

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Position"
        verbose_name_plural = "Positions"


class Formazione(models.Model):
    desc = models.CharField(max_length=500)

    def __str__(self):
        return self.desc

    class Meta:
        verbose_name = "Formazione"
        verbose_name_plural = "Formaziones"


class Utente(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user', unique=True)
    area = models.CharField(max_length=500, null=True)
    rank = models.ForeignKey(Rank, on_delete=models.DO_NOTHING, null=True)
    position = models.ForeignKey(Position, on_delete=models.DO_NOTHING, null=True)
    formazione = models.ForeignKey(Formazione, on_delete=models.DO_NOTHING, null=True)
    resposabile = models.ForeignKey(User, on_delete=models.CASCADE, related_name='responsabile')

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Dipendente"
        verbose_name_plural = "Dipendenti"