from django.shortcuts import render
from core.models import Utente
from corso.models import *
from valutazione.models import Skill


def addmult(request):
    lista_vendor = []
    lista_tech = None
    lista_user_select = ''
    lista_ambito_select = []
    if request.method == 'POST':
        post = request.POST
        print(post)
        lista_user_select = post['user']
        a = Anello1.objects.get(pk=post['ambito'])
        lista_ambito_select = [a.id, a.desc]
        if 'ambito' in post:
            s = Skill.objects.filter(ambito__id=post['ambito'])
            for x in s:
                lista_vendor.append(x.vendor.desc)
            print(lista_vendor)
            lista_vendor = list(set(lista_vendor))

    print(lista_vendor)
    data_to_template = {
        'nav' 		: '',
        'subnav' 		: '',
        'title_b' 	: 'Aggiunge Utente Skill',
        'title_sma' 	: "",
        'html_headr'	: '<li><a href="#">Il mio profilo</a></li>',
        'lista_user': Utente.objects.all().order_by('user__username'),
        'lista_ambito': Anello1.objects.all().order_by('desc'),
        'lista_vendor': lista_vendor,
        'lista_tech': lista_tech,
        'lista_user_select': lista_user_select,
        'lista_ambito_select': lista_ambito_select,
    }

    return render(request, 'core/addmult.html',  data_to_template)
