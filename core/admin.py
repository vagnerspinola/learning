from django.contrib import admin
from .models import *

class RankOption(admin.ModelAdmin):
    list_display   = ('id', 'desc')
    ordering       = ('id', )
    search_fields = ('id', 'desc')
    save_on_top    = True


class PositionOption(admin.ModelAdmin):
    list_display   = ('id', 'desc')
    ordering       = ('id', )
    search_fields = ('id', 'desc')
    save_on_top    = True


class FormazioneOption(admin.ModelAdmin):
    list_display = ('id', 'desc')
    ordering = ('id',)
    search_fields = ('id', 'desc')
    save_on_top = True


class UtenteOption(admin.ModelAdmin):
    list_display = ('id', 'user', 'rank', 'formazione', 'position', 'resposabile', 'area')
    ordering = ('id',)
    search_fields = ('id', 'name', 'surname')
    save_on_top = True


admin.site.register(Rank, RankOption)
admin.site.register(Position, PositionOption)
admin.site.register(Formazione, FormazioneOption)
admin.site.register(Utente, UtenteOption)